# CHANGELOG

This project adheres to [Semantic Versioning](http://semver.org/).
Which is based on [Keep A Changelog](http://keepachangelog.com/)

## Unreleased

### Added

- test: add support debian 12

### Changed

- test: use personal docker registry

### Removed

- test: removed support debian 10

### Fixed

- new grafana repository

## 1.1.0 - 2021-08-22

### Added

- feat: install unofficial plugins
- test: add support debian 11

### Changed

- chore: use FQCN for module name
- test: replace kitchen to molecule

### Removed

- test: remove support debian 9

## 1.0.1 - 2019-08-17

### Fixed

- default value for grafana_plugins is empty array

## 1.0.0 - 2019-04-11

- first version
