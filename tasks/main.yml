---
- name: Install dependencies packages
  ansible.builtin.package:
    name:
      - apt-transport-https
      - gpg
      - unzip
  tags: grafana

- name: Add repository key
  ansible.builtin.get_url:
    url: https://apt.grafana.com/gpg.key
    dest: /usr/share/keyrings/grafana.key
    owner: root
    group: root
    mode: 0644
  tags: grafana

- name: Add repository
  ansible.builtin.apt_repository:
    repo: deb [signed-by=/usr/share/keyrings/grafana.key] https://apt.grafana.com stable main
    filename: grafana
  tags: grafana

- name: Install grafana package
  ansible.builtin.package:
    name:
      - grafana
  tags: grafana

- name: Copy default environment variables file
  ansible.builtin.template:
    src: default.j2
    dest: /etc/default/grafana-server
    owner: root
    group: root
    mode: 0644
  notify: Restart grafana
  tags: grafana

- name: Copy configuration file
  ansible.builtin.template:
    src: grafana.ini.j2
    dest: /etc/grafana/grafana.ini
    owner: root
    group: grafana
    mode: 0640
  notify: Restart grafana
  tags: grafana

- name: Copy ldap configuration file
  ansible.builtin.template:
    src: ldap.toml.j2
    dest: /etc/grafana/ldap.toml
    owner: root
    group: grafana
    mode: 0640
  notify: Restart grafana
  tags: grafana

- name: Install official plugins
  community.grafana.grafana_plugin:
    name: "{{ item.name }}"
    version: "{{ item.version | default('latest') }}"
    state: "{{ item.state | default('present') }}"
  loop: "{{ grafana_plugins | selectattr('url', 'undefined') }}"
  loop_control:
    label: "{{ item.name }}"
  notify: Restart grafana
  tags: grafana

- name: Install unofficial plugins
  community.grafana.grafana_plugin:
    name: "{{ item.name }}"
    grafana_plugin_url: "{{ item.url }}"
    version: "{{ item.version | default('latest') }}"
    state: "{{ item.state | default('present') }}"
  loop: "{{ grafana_plugins | selectattr('url', 'defined') }}"
  loop_control:
    label: "{{ item.name }}"
  notify: Restart grafana
  tags: grafana

- name: Enable and start service
  ansible.builtin.service:
    name: grafana-server
    state: started
    enabled: true
  tags: grafana
