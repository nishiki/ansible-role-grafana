import testinfra.utils.ansible_runner

def test_packages(host):
  package = host.package('grafana')
  assert package.is_installed

def test_config_file(host):
  path = host.file('/etc/grafana/grafana.ini')
  assert path.exists
  assert path.is_file
  assert path.user == 'root'
  assert path.group == 'grafana'
  assert path.mode == 0o640
  assert not path.contains('default')
  assert path.contains('instance_name = "${HOSTNAME}"')
  assert path.contains('[security]')
  assert path.contains('admin_user = "sysadmin"')

def test_default_config_file(host):
  path = host.file('/etc/default/grafana-server')
  assert path.exists
  assert path.is_file
  assert path.user == 'root'
  assert path.group == 'root'
  assert path.mode == 0o644
  assert path.contains('LOG_DIR=/var/log/grafana')

def test_ldap_config_file(host):
  path = host.file('/etc/grafana/ldap.toml')
  assert path.exists
  assert path.is_file
  assert path.user == 'root'
  assert path.group == 'grafana'
  assert path.mode == 0o640
 
def test_plugins_install(host):
  for plugin_name in ['grafana-piechart-panel', 'sensu-sensugo-datasource']:
    path = host.file('/var/lib/grafana/plugins/%s' % plugin_name)
    assert path.exists
    assert path.is_directory
    assert path.user == 'root'
 
def test_service(host):
  service = host.service('grafana-server')
  assert service.is_running
  assert service.is_enabled

def test_socket(host):
  socket = host.socket('tcp://0.0.0.0:3000')
  assert socket.is_listening
