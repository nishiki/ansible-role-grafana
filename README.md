# Ansible role: Grafana

[![Version](https://img.shields.io/badge/latest_version-1.1.0-green.svg)](https://code.waks.be/nishiki/ansible-role-grafana/releases)
[![License](https://img.shields.io/badge/license-Apache--2.0-blue.svg)](https://code.waks.be/nishiki/ansible-role-sensu/src/branch/main/LICENSE)
[![Build](https://code.waks.be/nishiki/ansible-role-grafana/actions/workflows/molecule.yml/badge.svg?branch=main)](https://code.waks.be/nishiki/ansible-role-grafana/actions?workflow=molecule.yml)

Install and configure Grafana

## Requirements

- Ansible >= 2.9
- Debian
  - Buster
  - Bullseye

## Role variables

- `grafana_config` - hash with the grafana configuration (see [grafana documentation](http://docs.grafana.org/installation/configuration/))

```
  default:
    instance_name: '${HOSTNAME}'
  security:
    admin_user: sysadmin
    admin_password: secret
```

- `grafana_ldap_config` - hash with ldap configuration (see [grafana with ldap](http://docs.grafana.org/auth/ldap/))
- `grafana_plugins` - array with grafana plugins (see [grafana plugins](https://grafana.com/plugins))

```
  - name: grafana-piechart-panel
    version: 1.3.6
    state: present
  - name: sensu-sensugo-datasource
    url: https://github.com/sensu/grafana-sensu-go-datasource/releases/download/1.0.2/sensu-sensugo-datasource-1.0.2.zip
```

- `grafana_proxy_url` - set an URL proxy for outbound http and https requests
- `grafana_proxy_ignore` - array with subnets or hosts to ignore

```
  - localhost
  - 10.0.0.0/8
```

## How to use

```
- hosts: server
  roles:
    - grafana
```

## Development

### Test with molecule and docker

- install [docker](https://docs.docker.com/engine/installation/)
- install `python3` and `python3-pip`
- install molecule and dependencies `pip3 install molecule molecule-docker docker ansible-lint pytest-testinfra yamllint`
- run `molecule test`

## License

```
Copyright (c) 2019 Adrien Waksberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
